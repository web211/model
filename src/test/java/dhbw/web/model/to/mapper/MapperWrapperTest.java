package dhbw.web.model.to.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.junit.Test;

import dhbw.web.model.to.mapper.test.ObjTest;
import dhbw.web.model.to.mapper.test.ObjTestTO;
import dhbw.web.utils.MapperWrapper;

public class MapperWrapperTest {

	private final String packagename = "dhbw.web.model.to.mapper.test";

	@Test
	public void RelfactionTest() {

		// Act
		MapperWrapper mw = new MapperWrapper(packagename);

		// Assert
		assertEquals("did not find the expected amount of Mapper", 2, mw.getMapper().size());
	}

	@Test
	public void MapTest() {
		// Arrange
		MapperWrapper mw = new MapperWrapper(packagename + ".oneway");
		ObjTest o = new ObjTest();
		String value = "test";
		o.setName(value);

		// Act
		ObjTestTO to = mw.map(o, ObjTestTO.class);

		// Assert
		assertNotNull("returned a null value", to);
		assertEquals("did not convert the value into the new object", value, to.getName());
	}

	@Test
	public void RevertTest() {
		// Arrange
		MapperWrapper mw = new MapperWrapper(packagename + ".twoway");
		ObjTestTO to = new ObjTestTO();
		String value = "test";
		to.setName(value);

		// Act
		ObjTest o = mw.map(to, ObjTest.class);

		// Assert
		assertNotNull("returned a null value", o);
		assertEquals("did not convert the value into the new object", value, o.getName());
	}

	@Test(expected = OperationNotSupportedException.class)
	public void NoMapperTest() throws Throwable {
		// Arrange
		MapperWrapper mw = new MapperWrapper(packagename);

		// Act
		try {
			mw.map(new Object(), Object.class);
		} catch (Error e) {
			if (e.getCause() != null) {
				throw e.getCause();
			}
		}
	}

	@Test
	public void MapIterator() {
		// Arrange
		MapperWrapper mw = new MapperWrapper(packagename);
		String value = "test";
		List<ObjTest> list = new ArrayList<>();
		list.add(new ObjTest(value));
		list.add(new ObjTest(value));

		// Act
		List<ObjTestTO> toList = mw.map(list, ObjTest.class, ObjTestTO.class);

		// Assert
		assertEquals("The mapped list does not habe the correct amount of elements", 2, toList.size());
		for (int i = 0; i < toList.size(); i++) {
			ObjTestTO objTestTO = toList.get(i);
			assertEquals("did not convert the value into the new object, index: " + i, value, objTestTO.getName());
		}
	}

	@Test
	public void MapArray() {
		// Arrange
		MapperWrapper mw = new MapperWrapper(packagename);
		String value = "test";
		ObjTest[] array = new ObjTest[] { new ObjTest(value), new ObjTest(value) };

		// Act
		List<ObjTestTO> toList = mw.map(array, ObjTestTO.class);

		// Assert
		assertEquals("The mapped list does not habe the correct amount of elements", 2, toList.size());
		for (int i = 0; i < toList.size(); i++) {
			ObjTestTO objTestTO = toList.get(i);
			assertEquals("did not convert the value into the new object, index: " + i, value, objTestTO.getName());
		}
	}
}
