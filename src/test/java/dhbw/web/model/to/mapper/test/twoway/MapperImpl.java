package dhbw.web.model.to.mapper.test.twoway;

import dhbw.web.interfaces.Mapper;
import dhbw.web.model.to.mapper.test.ObjTest;
import dhbw.web.model.to.mapper.test.ObjTestTO;
import dhbw.web.utils.MapperWrapper;

public class MapperImpl extends Mapper<ObjTest, ObjTestTO> {

	@Override
	protected MapperWrapper constrctMapper() {
		return null;
	}

	@Override
	public ObjTestTO map(ObjTest o) {
		ObjTestTO to = new ObjTestTO();
		to.setName(o.getName());
		return to;
	}

	@Override
	public ObjTest revert(ObjTestTO to) {
		ObjTest o = new ObjTest();
		o.setName(to.getName());
		return o;
	}

}
