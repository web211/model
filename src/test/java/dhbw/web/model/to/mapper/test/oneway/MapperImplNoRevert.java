package dhbw.web.model.to.mapper.test.oneway;

import dhbw.web.interfaces.Mapper;
import dhbw.web.model.to.mapper.test.ObjTest;
import dhbw.web.model.to.mapper.test.ObjTestTO;
import dhbw.web.utils.MapperWrapper;

public class MapperImplNoRevert extends Mapper<ObjTest, ObjTestTO> {

	@Override
	protected MapperWrapper constrctMapper() {
		return null;
	}

	@Override
	public ObjTestTO map(ObjTest o) {
		ObjTestTO to = new ObjTestTO();
		to.setName(o.getName());
		return to;
	}

}
