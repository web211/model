package dhbw.web.interfaces;

/**
 * This interface is intended to provide a method which copies all attributes
 * from an object of the same instance to the current object.
 * 
 * @author nivogel
 *
 * @param <T>
 */
public interface CopyAttributes<T> {

	/**
	 * copy all attributes from source to the object which implements this method
	 * 
	 * @param source read the information from here.
	 */
	void copyAttributes(T source);

}
