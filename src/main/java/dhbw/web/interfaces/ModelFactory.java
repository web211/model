package dhbw.web.interfaces;

import java.util.List;

import dhbw.web.model.ModelFactoryShare;

public interface ModelFactory {

	public static ModelFactory getFactory() {
		return ModelFactoryShare.getFactory();
	}

	/**
	 * create a new list
	 * 
	 * @return new list
	 */
	<T> List<T> getList();

	/**
	 * create a clone of a list
	 * 
	 * @param elements copy all references from this list
	 * @return a new list with the same elements
	 */
	<T> List<T> getListClone(List<T> elements);
}
