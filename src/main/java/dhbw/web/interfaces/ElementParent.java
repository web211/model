package dhbw.web.interfaces;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import dhbw.web.model.Answer;
import dhbw.web.model.Comment;
import dhbw.web.model.ElementParentImpl;
import dhbw.web.model.Question;
import dhbw.web.model.User;

/**
 * is used by comments and
 * 
 * @author nivogel
 *
 */
@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = Answer.class, name = "a"),
		@JsonSubTypes.Type(value = Comment.class, name = "c"), @JsonSubTypes.Type(value = Question.class, name = "q"),
		@JsonSubTypes.Type(value = User.class, name = "u"),
		@JsonSubTypes.Type(value = ElementParentImpl.class, name = "e") })
public interface ElementParent {

	/**
	 * get the ID of the parent
	 * 
	 * @return
	 */
	int getId();

	/**
	 * get the type of the parent
	 * 
	 * @return
	 */
	String getType();

}
