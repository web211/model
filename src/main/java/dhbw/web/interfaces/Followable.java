package dhbw.web.interfaces;

import java.util.List;

import dhbw.web.model.Follow;
import dhbw.web.model.User;

/**
 * provides the functionality to follow something
 * 
 * @author nivogel
 *
 */
public interface Followable extends ElementParent {

	/**
	 * follow this element
	 * 
	 * @param user that user follows the element
	 * @return a new {@link Follow} object if the user was not a follower before,
	 *         otherwise null
	 */
	Follow follow(User user);

	/**
	 * unfollow this element
	 * 
	 * @param follow remove this follow object
	 * @return true if the follow was removed and false if nothing changed.
	 */
	boolean unfollow(Follow follow);

	/**
	 * unfollow this element
	 * 
	 * @param user unfollow with this user from the element
	 * @return the follow which was removed or null if the user did not follow this
	 *         element.
	 */
	Follow unfollow(User user);

	/**
	 * get all follows from this element. It does not return the original list, but
	 * a clone instead.
	 * 
	 * @return a clone of the list of all follows
	 */
	List<Follow> getAllFollower();
}
