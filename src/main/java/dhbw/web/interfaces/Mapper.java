package dhbw.web.interfaces;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.naming.OperationNotSupportedException;

import dhbw.web.utils.MapperWrapper;
import lombok.Getter;
import lombok.SneakyThrows;

public abstract class Mapper<FROM, TO> {

	@Getter
	private Type fromType;
	@Getter
	private Type toType;

	private MapperWrapper mw;

	protected MapperWrapper getMapper() {
		if (mw == null) {
			mw = constrctMapper();
		}
		return mw;
	}

	protected abstract MapperWrapper constrctMapper();

	public Mapper() {
		this.fromType = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		this.toType = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
	}

	public abstract TO map(FROM o);

	@SneakyThrows
	public FROM revert(TO t) {
		throw new OperationNotSupportedException(String.format("There is no implementation to cast from %s to %s",
				this.toType.getTypeName(), this.fromType.getTypeName()));
	}
}
