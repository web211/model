package dhbw.web.interfaces;

import java.util.List;

import dhbw.web.model.Comment;
import dhbw.web.model.User;

/**
 * provides the functionality to comment something
 * 
 * @author nivogel
 *
 */
public interface Commentable extends ElementParent {

	/**
	 * add a comment to this element
	 * 
	 * @param user this user created the comment
	 * @param text text for the comment
	 * @return a new {@link Comment} which was added to this element
	 */
	Comment addComment(User user, String text);

	/**
	 * remove a comment from this element
	 * 
	 * @param comment remove this comment
	 * @return true if the comment was remove, else false
	 */
	boolean removeComment(Comment comment);

	/**
	 * update a comment from this element
	 * 
	 * @param comment update this comment
	 * @exception IllegalArgumentException if the comment is not part of this
	 *                                     element!
	 */
	void updateComment(Comment comment);

	/**
	 * get a list of all comments from this element. The list is a clone of the
	 * internal list.
	 * 
	 * @return get a cloned list with all comments from this element.
	 */
	List<Comment> getAllComments();

}
