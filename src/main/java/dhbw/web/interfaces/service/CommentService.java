package dhbw.web.interfaces.service;

import dhbw.web.model.Comment;
import dhbw.web.model.form.CreateCommentForm;
import dhbw.web.model.to.CommentTO;

public interface CommentService {

	Comment[] getElemnetComments(int id, String type);

	Comment[] getAllComments();

	Comment createComment(CreateCommentForm form);

	Comment readComment(int id);

	void updateComment(CommentTO comment);

	void deleteComment(int id);

}
