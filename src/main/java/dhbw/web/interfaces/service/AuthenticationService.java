package dhbw.web.interfaces.service;

import dhbw.web.model.User;

public interface AuthenticationService {

	void save(User user);

	User findByUsername(String username);
}
