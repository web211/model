package dhbw.web.interfaces.service;

import dhbw.web.model.Question;
import dhbw.web.model.form.CreateQuestionForm;
import dhbw.web.model.to.QuestionTO;

public interface QuestionService {

	Question[] getAllQuestions();

	Question createQuestion(CreateQuestionForm form);

	Question readQuestionByUrl(String url);

	Question readQuestion(int id);

	void updateQuestion(QuestionTO question);

	void deleteQuestion(int id);

}
