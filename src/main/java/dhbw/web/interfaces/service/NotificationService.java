package dhbw.web.interfaces.service;

import dhbw.web.model.Notification;
import dhbw.web.model.form.PushNotificationForm;

public interface NotificationService {

	Notification[] publish(PushNotificationForm form);

	Notification[] getLatestUserNotifications(int userId, int amount);

	Notification[] getAllUserNotifications(int userId);

}
