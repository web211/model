package dhbw.web.interfaces.service;

import dhbw.web.model.User;
import dhbw.web.model.to.UserTO;

public interface UserService {

	User[] readAllUser();

	User createUser(UserTO user);

	User readUserByUsername(String username);

	User readUser(int id);

	void updateUser(UserTO user);

	void deleteUser(int id);
}
