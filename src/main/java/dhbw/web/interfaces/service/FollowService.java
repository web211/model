package dhbw.web.interfaces.service;

import dhbw.web.model.Follow;
import dhbw.web.model.form.ToggleFollowForm;

public interface FollowService {

	Follow toggleFollow(ToggleFollowForm form);

	Follow[] readFollow(int element, String type, int userId);

	Follow[] readAllFollower();

}
