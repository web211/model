package dhbw.web.interfaces.service;

import dhbw.web.model.Like;
import dhbw.web.model.form.ToggleLikeForm;

public interface LikeService {

	Like toggleLike(ToggleLikeForm form);

	Like[] readLike(int element, String type, int userId);

	Like[] readAllLikes();

}
