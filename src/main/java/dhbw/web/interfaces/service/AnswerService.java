package dhbw.web.interfaces.service;

import dhbw.web.model.Answer;
import dhbw.web.model.form.CreateAnswerForm;
import dhbw.web.model.to.AnswerTO;

public interface AnswerService {

	Answer createAnswer(CreateAnswerForm form);

	Answer[] getQuestionAnswers(int id);

	Answer[] readAllAnswers();

	Answer readAnswer(int id);

	void updateAnswer(AnswerTO answer);

	void deleteAnswer(int id);

}
