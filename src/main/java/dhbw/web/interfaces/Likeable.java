package dhbw.web.interfaces;

import java.util.List;

import dhbw.web.model.Like;
import dhbw.web.model.User;

/**
 * provides the functionality to like something
 * 
 * @author nivogel
 *
 */
public interface Likeable extends ElementParent {

	/**
	 * like this element
	 * 
	 * @param user that user likes this element
	 * @return a new {@link Like} object if the user did not like this element
	 *         before, otherwise null.
	 */
	Like like(User user);

	/**
	 * unlike this element
	 * 
	 * @param like remove this like from the element
	 * @return true if the like was removed and false if nothing changed.
	 */
	boolean unlike(Like like);

	/**
	 * unlike this element
	 * 
	 * @param user unlike with this user this element
	 * @return the element which was removed or null if the user did not like the
	 *         element.
	 */
	Like unlike(User user);

	/**
	 * get all likes from this element. It does not return the original list, but a
	 * clone instead.
	 * 
	 * @return a clone of the list of all likes
	 */
	List<Like> getAllLikes();
}
