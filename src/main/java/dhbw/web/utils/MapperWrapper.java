package dhbw.web.utils;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import dhbw.web.interfaces.Mapper;
import dhbw.web.interfaces.ModelFactory;
import lombok.Getter;

@SuppressWarnings("rawtypes")
public class MapperWrapper {

	@Getter
	private List<Mapper> mapper;
	@Getter
	private String packageName;

	public MapperWrapper(String packageName) {
		this.packageName = packageName;
		this.mapper = ModelFactory.getFactory().getList();
		ClassFinder.addExtendedClasses(Mapper.class, packageName, this.mapper);
	}

	public <T1, T2> T2 map(T1 o, Class<T2> to) {
		MapperPair<T1, T2> pair = getMapperPair(o.getClass(), to);
		pairCheck(pair);
		return map(pair, o);
	}

	@SuppressWarnings("unchecked")
	public <T1, T2> List<T2> map(T1[] array, Class<T2> to) {
		return map(Arrays.asList(array), (Class<T1>) array.getClass().getComponentType(), to);
	}

	public <T1, T2> List<T2> map(Iterable<T1> iterator, Class<T1> from, Class<T2> to) {
		MapperPair<T1, T2> pair = getMapperPair(from, to);
		pairCheck(pair);
		List<T2> result = ModelFactory.getFactory().getList();
		for (T1 o : iterator) {
			result.add(map(pair, o));
		}
		return result;
	}

	public void addMapper(Mapper m) {
		this.mapper.add(m);
	}

	private Mapper getMapper(Type from, Type to) {
		for (int i = 0; i < this.mapper.size(); i++) {
			Mapper cur = this.mapper.get(i);
			if (cur.getFromType() == from && cur.getToType() == to) {
				return cur;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private <T1, T2> MapperPair<T1, T2> getMapperPair(Type from, Type to) {
		MapperPair<T1, T2> pair = new MapperPair<>();
		pair.mapperTO = getMapper(from, to);
		pair.mapperFROM = pair.mapperTO == null ? getMapper(to, from) : null;
		pair.from = from.toString();
		pair.to = to.toString();
		return pair;
	}

	private <T1, T2> T2 map(MapperPair<T1, T2> pair, T1 from) {
		if (!pair.toNull()) {
			return pair.mapperTO.map(from);
		}
		return pair.mapperFROM.revert(from);
	}

	private void pairCheck(MapperPair pair) {
		if (pair.areNull()) {
			String text = "No mapper is availiable for " + pair.from + " to " + pair.to;
			throw new Error(text, new OperationNotSupportedException(text));
		}
	}

	private class MapperPair<T1, T2> {
		Mapper<T1, T2> mapperTO;
		Mapper<T2, T1> mapperFROM;
		String from;
		String to;

		boolean areNull() {
			return mapperTO == null && mapperFROM == null;
		}

		boolean toNull() {
			return mapperTO == null;
		}
	}
}
