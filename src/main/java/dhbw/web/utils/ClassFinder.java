package dhbw.web.utils;

import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Set;

import net.sourceforge.stripes.util.ResolverUtil;

public class ClassFinder {

	private ClassFinder() {

	}

	@SuppressWarnings("unchecked")
	public static <T> void addExtendedClasses(Class<T> clazz, String packageName, List<T> output) {
		for (Class<? extends T> classAufgabe : getExtendedClasses(clazz, packageName)) {
			try {
				if (!classAufgabe.equals(clazz) && !Modifier.isAbstract(classAufgabe.getModifiers())) {
					Object obj = classAufgabe.newInstance();
					if (clazz.isInstance(obj)) {
						output.add((T) obj);
					}
				}
			} catch (Exception e) {
				throw new Error(e.getMessage(), e);
			}
		}
	}

	public static <T> Set<Class<? extends T>> getExtendedClasses(Class<T> clazz, String packageName) {
		try {
			ResolverUtil<T> resolver = new ResolverUtil<>();
			resolver.findImplementations(clazz, packageName);
			return resolver.getClasses();
		} catch (Exception e) {
			throw new Error(e.getMessage(), e);
		}
	}

}
