package dhbw.web.model;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Notification extends Entity {

	private static final long serialVersionUID = -1601062224444341184L;
	private Date created;
	private String text;
	private Follow follow;
	private boolean seen;
}
