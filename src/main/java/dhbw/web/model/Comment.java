package dhbw.web.model;

import java.util.Date;
import java.util.List;

import dhbw.web.interfaces.CopyAttributes;
import dhbw.web.interfaces.ElementParent;
import dhbw.web.interfaces.Likeable;
import dhbw.web.model.lists.LikeList;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * represents one comment from the forum
 * 
 * @author nivogel
 *
 */
@EqualsAndHashCode(callSuper = true)
@ToString
public class Comment extends Entity implements Likeable, CopyAttributes<Comment> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2601509399887733182L;
	public static final String TYPE = "c";

	@Getter
	@Setter
	private String text;
	@Getter
	@Setter
	private Date created;
	@Getter
	@Setter
	private User owner;
	@Getter
	@Setter
	private ElementParent parent;
	private LikeList likeList;

	public Comment() {
		this.likeList = new LikeList(this);
	}

	public Comment(String text, User owner, ElementParent parent) {
		this(text, owner, parent, new Date());
	}

	public Comment(String text, User owner, ElementParent parent, Date created) {
		this();
		this.text = text;
		this.created = created;
		this.owner = owner;
		this.parent = parent;
	}

	@Override
	public Like like(User user) {
		return this.likeList.like(user);
	}

	@Override
	public boolean unlike(Like like) {
		return this.likeList.unlike(like);
	}

	@Override
	public Like unlike(User user) {
		return this.likeList.unlike(user);
	}

	@Override
	public List<Like> getAllLikes() {
		return this.likeList.getAllLikes();
	}

	@Override
	public void copyAttributes(Comment source) {
		this.setText(source.getText());
		this.setOwner(source.getOwner());
		this.setCreated(source.getCreated());
	}

	@Override
	public String getType() {
		return TYPE;
	}

}
