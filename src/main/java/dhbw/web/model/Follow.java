package dhbw.web.model;

import java.util.Date;

import dhbw.web.interfaces.ElementParent;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * This class represents one follow
 * 
 * @author nivogel
 *
 */
@EqualsAndHashCode(callSuper = true)
@ToString
public class Follow extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3680349823777233452L;

	@Getter
	@Setter
	private User follower;
	@Getter
	@Setter
	private ElementParent parent;
	@Getter
	@Setter
	private Date created;

	/**
	 * for JPA
	 */
	public Follow() {
	}

	public Follow(User follower, ElementParent parent) {
		this(follower, parent, new Date());
	}

	public Follow(User follower, ElementParent parent, Date created) {
		this.follower = follower;
		this.parent = parent;
		this.created = created;
	}

}
