package dhbw.web.model;

import java.util.Date;

import dhbw.web.interfaces.ElementParent;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * This class represents one like.
 * 
 * @author nivogel
 *
 */
@EqualsAndHashCode(callSuper = true)
@ToString
@NoArgsConstructor
public class Like extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4474931058899615789L;

	@Getter
	@Setter
	private User liked;
	@Getter
	@Setter
	private ElementParent parent;
	@Getter
	@Setter
	private Date created;

	public Like(User liked, ElementParent element) {
		this(liked, element, new Date());
	}

	public Like(User liked, ElementParent element, Date created) {
		setLiked(liked);
		setParent(element);
		setCreated(created);
	}

}
