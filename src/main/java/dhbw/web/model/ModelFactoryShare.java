package dhbw.web.model;

import dhbw.web.interfaces.ModelFactory;

public class ModelFactoryShare {

	public static ModelFactory factory;

	public static ModelFactory getFactory() {
		if (factory == null) {
			factory = new ModelFactoryImpl();
		}
		return factory;
	}

}
