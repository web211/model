package dhbw.web.model.to;

import java.util.Date;

import lombok.Data;

@Data
public class CommentTO {

	private int id;
	private String text;
	private Date created;
	private int userId;
	private String username;
	private int likeCount;
	private int parentId;
	private String parentType;

}
