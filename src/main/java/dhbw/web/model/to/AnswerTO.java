package dhbw.web.model.to;

import java.util.Date;
import java.util.List;

import dhbw.web.interfaces.ModelFactory;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
public class AnswerTO {

	private int id;
	private String text;
	private int userId;
	private String username;
	private Date created;
	private int parentQuestionId;
	@Setter(value = AccessLevel.PRIVATE)
	private List<CommentTO> comments;
	private int likeCount;

	public List<CommentTO> getComments() {
		if (this.comments == null) {
			this.comments = ModelFactory.getFactory().getList();
		}
		return this.comments;
	}

}
