package dhbw.web.model.to;

import java.util.Date;

import lombok.Data;

@Data
public class NotificationTO {

	private int id;
	private Date created;
	private String text;
	private FollowTO follow;
	private int parentId;
	private String parentType;
	private boolean seen;

}
