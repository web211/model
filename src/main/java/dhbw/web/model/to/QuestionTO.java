package dhbw.web.model.to;

import java.util.Date;
import java.util.List;

import dhbw.web.interfaces.ModelFactory;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
public class QuestionTO {

	private int id;
	private String title;
	private String text;
	private int userId;
	private String username;
	private Date created;
	private String url;
	@Setter(value = AccessLevel.PRIVATE)
	private List<AnswerTO> answers;
	@Setter(value = AccessLevel.PRIVATE)
	private List<CommentTO> comments;
	@Setter(value = AccessLevel.PRIVATE)
	private List<FollowTO> follower;
	private int likeCount;

	public List<AnswerTO> getAnswers() {
		if (this.answers == null) {
			this.answers = ModelFactory.getFactory().getList();
		}
		return this.answers;
	}

	public List<CommentTO> getComments() {
		if (this.comments == null) {
			this.comments = ModelFactory.getFactory().getList();
		}
		return this.comments;
	}

	public List<FollowTO> getFollower() {
		if (this.follower == null) {
			this.follower = ModelFactory.getFactory().getList();
		}
		return this.follower;
	}
}
