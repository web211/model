package dhbw.web.model.to;

import java.util.Date;

import lombok.Data;

@Data
public class LikeTO {

	private int id;
	private int userId;
	private int parentId;
	private String parentType;
	private Date created;
}
