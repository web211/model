package dhbw.web.model.to;

import java.util.Date;
import java.util.List;

import dhbw.web.interfaces.ModelFactory;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

/**
 * this is a transport object which hides the underlying code structure and does
 * only contain data information without any logic
 * 
 * @author nivogel
 *
 */
@Data
public class UserTO {
	private int id;
	private String username;
	private String email;
	private String firstname;
	private String lastname;
	private String password;
	private Date lastLogon;
	private Date created;
	@Setter(value = AccessLevel.PRIVATE)
	private List<CommentTO> comments;
	@Setter(value = AccessLevel.PRIVATE)
	private List<FollowTO> follows;
	@Setter(value = AccessLevel.PRIVATE)
	private List<LikeTO> likes;

	public List<CommentTO> getComments() {
		if (this.comments == null) {
			this.comments = ModelFactory.getFactory().getList();
		}
		return this.comments;
	}

	public List<FollowTO> getFollows() {
		if (this.follows == null) {
			this.follows = ModelFactory.getFactory().getList();
		}
		return this.follows;
	}

	public List<LikeTO> getLikes() {
		if (this.likes == null) {
			this.likes = ModelFactory.getFactory().getList();
		}
		return this.likes;
	}
}
