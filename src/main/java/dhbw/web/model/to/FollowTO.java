package dhbw.web.model.to;

import java.util.Date;

import lombok.Data;

@Data
public class FollowTO {
	private int id;
	private int userId;
	private int parentId;
	private String parentType;
	private Date created;

}
