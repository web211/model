package dhbw.web.model.to.mapper;

import dhbw.web.model.Like;
import dhbw.web.model.to.LikeTO;

public class LikeTOMapper extends ModelTOSelfreference<Like, LikeTO> {

	@Override
	public LikeTO map(Like o) {
		LikeTO t = new LikeTO();
		t.setId(o.getId());
		t.setCreated(o.getCreated());
		t.setParentId(o.getParent().getId());
		t.setParentType(o.getParent().getType());
		t.setUserId(o.getLiked().getId());
		return t;
	}

}
