package dhbw.web.model.to.mapper;

import dhbw.web.model.Notification;
import dhbw.web.model.to.FollowTO;
import dhbw.web.model.to.NotificationTO;

public class NotificationTOMapper extends ModelTOSelfreference<Notification, NotificationTO> {

	@Override
	public NotificationTO map(Notification o) {
		NotificationTO t = new NotificationTO();
		t.setId(o.getId());
		t.setCreated(o.getCreated());
		t.setFollow(super.getMapper().map(o.getFollow(), FollowTO.class));
		t.setSeen(o.isSeen());
		t.setText(o.getText());
		return t;
	}

}
