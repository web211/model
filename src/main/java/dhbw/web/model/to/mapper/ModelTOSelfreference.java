package dhbw.web.model.to.mapper;

import dhbw.web.interfaces.Mapper;
import dhbw.web.utils.MapperWrapper;

public abstract class ModelTOSelfreference<FROM, TO> extends Mapper<FROM, TO> {

	@Override
	protected MapperWrapper constrctMapper() {
		return ModelTOMapper.getModelToMapper();
	}

}
