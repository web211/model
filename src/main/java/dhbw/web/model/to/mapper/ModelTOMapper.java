package dhbw.web.model.to.mapper;

import dhbw.web.utils.MapperWrapper;

public class ModelTOMapper {

	private static MapperWrapper mw;

	public static MapperWrapper getModelToMapper() {
		if (mw == null) {
			mw = new MapperWrapper("dhbw.web.model.to.mapper");
		}
		return mw;
	}

	private ModelTOMapper() {

	}

}
