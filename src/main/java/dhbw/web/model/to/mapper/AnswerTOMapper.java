package dhbw.web.model.to.mapper;

import java.util.stream.Collectors;

import dhbw.web.model.Answer;
import dhbw.web.model.to.AnswerTO;
import dhbw.web.model.to.CommentTO;

public class AnswerTOMapper extends ModelTOSelfreference<Answer, AnswerTO> {

	@Override
	public AnswerTO map(Answer o) {
		AnswerTO t = new AnswerTO();
		t.setId(o.getId());
		t.setText(o.getText());
		t.setCreated(o.getCreated());
		t.setUserId(o.getOwner().getId());
		t.setUsername(o.getOwner().getUsername());
		t.setParentQuestionId(o.getParentQuestion().getId());
		t.getComments().addAll(o.getAllComments().stream().map(x -> super.getMapper().map(x, CommentTO.class))
				.collect(Collectors.toList()));
		t.setLikeCount(o.getAllLikes().size());
		return t;
	}

}
