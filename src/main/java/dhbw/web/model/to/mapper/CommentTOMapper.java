package dhbw.web.model.to.mapper;

import dhbw.web.model.Comment;
import dhbw.web.model.to.CommentTO;

public class CommentTOMapper extends ModelTOSelfreference<Comment, CommentTO> {

	@Override
	public CommentTO map(Comment o) {
		CommentTO t = new CommentTO();
		t.setCreated(o.getCreated());
		t.setId(o.getId());
		t.setText(o.getText());
		t.setUserId(o.getOwner().getId());
		t.setUsername(o.getOwner().getUsername());
		t.setLikeCount(o.getAllLikes().size());
		t.setParentId(o.getParent().getId());
		t.setParentType(o.getParent().getType());
		return t;
	}

}
