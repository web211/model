package dhbw.web.model.to.mapper;

import dhbw.web.model.Follow;
import dhbw.web.model.to.FollowTO;

public class FollowTOMapper extends ModelTOSelfreference<Follow, FollowTO> {

	@Override
	public FollowTO map(Follow o) {
		FollowTO t = new FollowTO();
		t.setId(o.getId());
		t.setCreated(o.getCreated());
		t.setParentId(o.getParent().getId());
		t.setParentType(o.getParent().getType());
		t.setUserId(o.getFollower().getId());
		return t;
	}

}
