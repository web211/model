package dhbw.web.model.to.mapper;

import java.util.stream.Collectors;

import dhbw.web.model.User;
import dhbw.web.model.to.CommentTO;
import dhbw.web.model.to.FollowTO;
import dhbw.web.model.to.LikeTO;
import dhbw.web.model.to.UserTO;

public class UserTOMapper extends ModelTOSelfreference<User, UserTO> {

	@Override
	public UserTO map(User o) {
		UserTO t = new UserTO();
		t.setId(o.getId());
		t.setUsername(o.getUsername());
		t.setEmail(o.getEmail());
		t.setFirstname(o.getFirstname());
		t.setLastname(o.getLastname());
		t.setPassword(o.getPassword());
		t.setLastLogon(o.getLastLogon());
		t.setCreated(o.getCreated());
		t.getComments().addAll(o.getAllComments().stream().map(x -> super.getMapper().map(x, CommentTO.class))
				.collect(Collectors.toList()));
		t.getFollows().addAll(o.getAllFollower().stream().map(x -> super.getMapper().map(x, FollowTO.class))
				.collect(Collectors.toList()));
		t.getLikes().addAll(
				o.getAllLikes().stream().map(x -> super.getMapper().map(x, LikeTO.class)).collect(Collectors.toList()));
		return t;
	}

}
