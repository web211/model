package dhbw.web.model.to.mapper;

import java.util.stream.Collectors;

import dhbw.web.model.Question;
import dhbw.web.model.to.AnswerTO;
import dhbw.web.model.to.CommentTO;
import dhbw.web.model.to.FollowTO;
import dhbw.web.model.to.QuestionTO;

public class QuestionTOMapper extends ModelTOSelfreference<Question, QuestionTO> {

	@Override
	public QuestionTO map(Question o) {
		QuestionTO t = new QuestionTO();
		t.setId(o.getId());
		t.setCreated(o.getCreated());
		t.setText(o.getText());
		t.setTitle(o.getTitle());
		t.setUrl(o.getUrl());
		t.setUserId(o.getOwner().getId());
		t.setUsername(o.getOwner().getUsername());
		t.getAnswers().addAll(o.getAllAnswers().stream().map(x -> super.getMapper().map(x, AnswerTO.class))
				.collect(Collectors.toList()));
		t.getComments().addAll(o.getAllComments().stream().map(x -> super.getMapper().map(x, CommentTO.class))
				.collect(Collectors.toList()));
		t.getFollower().addAll(o.getAllFollower().stream().map(x -> super.getMapper().map(x, FollowTO.class))
				.collect(Collectors.toList()));
		t.setLikeCount(o.getAllLikes().size());
		return t;
	}
}
