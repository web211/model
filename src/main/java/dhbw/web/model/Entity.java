package dhbw.web.model;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Every element int the database does have its own id, therefore this element
 * is a shared property between all elements from the database.
 * 
 * @author nivogel
 *
 */
@EqualsAndHashCode
@ToString
public abstract class Entity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 17011659909847057L;

	@Getter
	@Setter
	protected int id;
}
