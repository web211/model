package dhbw.web.model;

import java.util.Date;
import java.util.List;

import dhbw.web.interfaces.Commentable;
import dhbw.web.interfaces.Followable;
import dhbw.web.interfaces.Likeable;
import dhbw.web.model.lists.CommentList;
import dhbw.web.model.lists.FollowList;
import dhbw.web.model.lists.LikeList;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString
public class User extends Entity implements Followable, Likeable, Commentable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5373265869383689563L;
	public static final String TYPE = "u";

	@Getter
	@Setter
	private String username;
	@Getter
	@Setter
	private String email;
	@Getter
	@Setter
	private String firstname;
	@Getter
	@Setter
	private String lastname;
	@Getter
	@Setter
	private String password;
	@Getter
	@Setter
	private Date lastLogon;
	@Getter
	@Setter
	private Date created;
	private LikeList likeList;
	private FollowList followList;
	private CommentList commentList;

	public User() {
		this.likeList = new LikeList(this);
		this.followList = new FollowList(this);
		this.commentList = new CommentList(this);
	}

	public User(String userName, String email, String firstName, String lastName) {
		this();
		this.setUsername(userName);
		this.setEmail(email);
		this.setFirstname(firstName);
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public Like like(User user) {
		return this.likeList.like(user);
	}

	@Override
	public boolean unlike(Like like) {
		return this.likeList.unlike(like);
	}

	@Override
	public Like unlike(User user) {
		return this.likeList.unlike(user);
	}

	@Override
	public List<Like> getAllLikes() {
		return this.likeList.getAllLikes();
	}

	@Override
	public Follow follow(User user) {
		return this.followList.follow(user);
	}

	@Override
	public boolean unfollow(Follow follow) {
		return this.followList.unfollow(follow);
	}

	@Override
	public Follow unfollow(User user) {
		return this.followList.unfollow(user);
	}

	@Override
	public List<Follow> getAllFollower() {
		return this.followList.getAllFollower();
	}

	@Override
	public Comment addComment(User user, String text) {
		return this.commentList.addComment(user, text);
	}

	@Override
	public boolean removeComment(Comment comment) {
		return this.commentList.removeComment(comment);
	}

	@Override
	public void updateComment(Comment comment) {
		this.commentList.updateComment(comment);
	}

	@Override
	public List<Comment> getAllComments() {
		return this.commentList.getAllComments();
	}

}
