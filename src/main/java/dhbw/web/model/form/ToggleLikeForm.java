package dhbw.web.model.form;

import dhbw.web.interfaces.ElementParent;
import dhbw.web.model.User;
import lombok.Data;

@Data
public class ToggleLikeForm {

	private ElementParent element;
	private User liked;
}
