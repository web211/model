package dhbw.web.model.form;

import java.util.Date;

import dhbw.web.interfaces.ElementParent;
import dhbw.web.model.User;
import lombok.Data;

@Data
public class PushNotificationForm {

	private ElementParent parent;
	private Date created;
	private NotificationType type;
	private User user;
	private int userId;

	public PushNotificationForm() {
		this.created = new Date();
	}

	public PushNotificationForm(ElementParent parent, User user, NotificationType type) {
		this();
		this.parent = parent;
		this.user = user;
		this.userId = user.getId();
		this.type = type;
	}

	public PushNotificationForm(ElementParent parent, int userId, NotificationType type) {
		this();
		this.parent = parent;
		this.userId = userId;
		this.type = type;
	}
}
