package dhbw.web.model.form;

import dhbw.web.interfaces.ElementParent;
import dhbw.web.model.User;
import lombok.Data;

@Data
public class ToggleFollowForm {

	private ElementParent element;
	private User follower;
}
