package dhbw.web.model.form;

import dhbw.web.interfaces.ElementParent;
import dhbw.web.model.User;
import dhbw.web.model.to.CommentTO;
import lombok.Data;

@Data
public class CreateCommentForm {

	private ElementParent parent;
	private User user;
	private CommentTO comment;

}
