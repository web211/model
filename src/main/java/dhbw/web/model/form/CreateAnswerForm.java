package dhbw.web.model.form;

import dhbw.web.model.Question;
import dhbw.web.model.User;
import dhbw.web.model.to.AnswerTO;
import lombok.Data;

@Data
public class CreateAnswerForm {

	private Question parent;
	private User user;
	private AnswerTO answer;

}
