package dhbw.web.model.form;

import dhbw.web.model.User;
import dhbw.web.model.to.QuestionTO;
import lombok.Data;

@Data
public class CreateQuestionForm {

	private User user;
	private QuestionTO question;

}
