package dhbw.web.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import dhbw.web.interfaces.Commentable;
import dhbw.web.interfaces.CopyAttributes;
import dhbw.web.interfaces.Likeable;
import dhbw.web.model.lists.CommentList;
import dhbw.web.model.lists.LikeList;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString
@NoArgsConstructor
public class Answer extends Entity implements Serializable, Likeable, Commentable, CopyAttributes<Answer> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3785455562223639956L;
	public static final String TYPE = "a";

	@Getter
	@Setter
	private String text;
	@Getter
	@Setter
	private User owner;
	@Getter
	@Setter
	private Date created;
	@Getter
	@Setter
	private Question parentQuestion;
	private LikeList likeList;
	private CommentList commentList;

	public Answer(String text, User owner, Question parentQuestion) {
		this(text, owner, parentQuestion, new Date());
	}

	public Answer(String text, User owner, Question parentQuestion, Date created) {
		this();
		this.text = text;
		this.owner = owner;
		this.parentQuestion = parentQuestion;
		this.created = created;
	}

	@Override
	public Comment addComment(User user, String text) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean removeComment(Comment comment) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void updateComment(Comment comment) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Comment> getAllComments() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Like like(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean unlike(Like like) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Like unlike(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Like> getAllLikes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void copyAttributes(Answer source) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getType() {
		return TYPE;
	}
}
