package dhbw.web.model;

import java.io.Serializable;

import dhbw.web.interfaces.ElementParent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ElementParentImpl implements ElementParent, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8494027130838637657L;

	public static final String TYPE = "e";

	private int id;
	private String type;

	public ElementParentImpl(ElementParent element) {
		this.id = element.getId();
		this.type = element.getType();
	}

}
