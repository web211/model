package dhbw.web.model.lists;

import java.io.Serializable;
import java.util.List;

import dhbw.web.interfaces.Commentable;
import dhbw.web.interfaces.ElementParent;
import dhbw.web.interfaces.ModelFactory;
import dhbw.web.model.Comment;
import dhbw.web.model.User;

/**
 * This is a default implementation of the {@link Commentable} interface
 * 
 * @author nivogel
 *
 */
public class CommentList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8153242046353553666L;
	private List<Comment> comments;
	private ElementParent parent;

	public CommentList(ElementParent parent) {
		this.comments = ModelFactory.getFactory().getList();
		this.parent = parent;
	}

	public Comment addComment(User user, String text) {
		Comment comment = new Comment(text, user, this.parent);
		this.comments.add(comment);
		return comment;
	}

	public boolean removeComment(Comment comment) {
		return this.comments.remove(comment);
	}

	public void updateComment(Comment comment) {
		Comment existingComment = getById(comment.getId());
		if (existingComment == null) {
			throw new IllegalArgumentException(
					"cannot update a comment which is not included in the list! comment: " + comment);
		}
		existingComment.copyAttributes(comment);
	}

	public List<Comment> getAllComments() {
		return ModelFactory.getFactory().getListClone(this.comments);
	}

	private Comment getById(int id) {
		return this.comments.stream().filter(c -> c.getId() == id).findFirst().get();
	}

}
