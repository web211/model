
package dhbw.web.model.lists;

import java.io.Serializable;
import java.util.List;

import dhbw.web.interfaces.ElementParent;
import dhbw.web.interfaces.Likeable;
import dhbw.web.interfaces.ModelFactory;
import dhbw.web.model.Like;
import dhbw.web.model.User;

/**
 * * This is a default implementation for the {@link Likeable} interface *
 * * @author nivogel *
 */
public class LikeList implements Serializable {
	/** * */
	private static final long serialVersionUID = -5854960845329687621L;
	private ElementParent parent;

	public LikeList(ElementParent parent) {
		this.likes = ModelFactory.getFactory().getList();
		this.parent = parent;
	}

	private List<Like> likes;

	public Like like(User user) {
		Like userLike = getUserLike(user);
		if (userLike != null) {
			return userLike;
		}
		Like like = new Like(user, this.parent);
		this.likes.add(like);
		return like;
	}

	public boolean unlike(Like like) {
		return this.likes.remove(like);
	}

	public Like unlike(User user) {
		Like userLike = getUserLike(user);
		if (userLike == null) {
			return null;
		}
		this.likes.remove(userLike);
		return userLike;
	}

	private Like getUserLike(User user) {
		return this.likes.stream().filter(l -> l.getLiked().equals(user)).findFirst().get();
	}

	public List<Like> getLikes() {
		return this.likes;
	}

	public List<Like> getAllLikes() {
		return ModelFactory.getFactory().getListClone(this.likes);
	}
}
