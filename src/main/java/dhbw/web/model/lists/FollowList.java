
package dhbw.web.model.lists;

import java.io.Serializable;
import java.util.List;

import dhbw.web.interfaces.ElementParent;
import dhbw.web.interfaces.Followable;
import dhbw.web.interfaces.ModelFactory;
import dhbw.web.model.Follow;
import dhbw.web.model.User;

/**
 * * This is a default implementation for the {@link Followable} interface *
 * * @author nivogel *
 */
public class FollowList implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<Follow> follows;
	private ElementParent parent;

	public FollowList(ElementParent parent) {
		this.follows = ModelFactory.getFactory().getList();
		this.parent = parent;
	}

	public Follow follow(User user) {
		Follow userFollow = getUserFollow(user);
		if (userFollow != null) {
			return userFollow;
		}
		Follow follow = new Follow(user, this.parent);
		this.follows.add(follow);
		return follow;
	}

	public boolean unfollow(Follow follow) {
		return this.follows.remove(follow);
	}

	public Follow unfollow(User user) {
		Follow userFollow = getUserFollow(user);
		if (userFollow == null) {
			return null;
		}
		this.follows.remove(userFollow);
		return userFollow;
	}

	public List<Follow> getAllFollower() {
		return ModelFactory.getFactory().getListClone(this.follows);
	}

	private Follow getUserFollow(User user) {
		return this.follows.stream().filter(f -> f.getFollower().equals(user)).findFirst().get();
	}
}
