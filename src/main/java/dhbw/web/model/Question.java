package dhbw.web.model;

import java.util.Date;
import java.util.List;

import dhbw.web.interfaces.Commentable;
import dhbw.web.interfaces.CopyAttributes;
import dhbw.web.interfaces.Followable;
import dhbw.web.interfaces.Likeable;
import dhbw.web.interfaces.ModelFactory;
import dhbw.web.model.lists.CommentList;
import dhbw.web.model.lists.FollowList;
import dhbw.web.model.lists.LikeList;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * represents one Question or Answer
 * 
 * @author nivogel
 *
 */
@EqualsAndHashCode(callSuper = true)
@ToString
public class Question extends Entity implements Likeable, Followable, Commentable, CopyAttributes<Question> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3964543130277874248L;
	public static final String TYPE = "q";

	@Getter
	@Setter
	private String title;
	@Getter
	@Setter
	private String text;
	@Getter
	@Setter
	private User owner;
	@Getter
	@Setter
	private Date created;
	@Getter
	@Setter
	private String url;
	private LikeList likeList;
	private FollowList followList;
	private CommentList commentList;
	private List<Answer> answers;

	/**
	 * for JPA
	 */
	public Question() {
		this.likeList = new LikeList(this);
		this.followList = new FollowList(this);
		this.commentList = new CommentList(this);
		this.answers = ModelFactory.getFactory().getList();
	}

	public Question(String title, String text, User owner) {
		this(title, text, owner, new Date());
	}

	public Question(String title, String text, User owner, Date created) {
		this();
		this.title = title;
		this.text = text;
		this.owner = owner;
		this.created = created;
	}

	@Override
	public Comment addComment(User user, String text) {
		return this.commentList.addComment(user, text);
	}

	@Override
	public boolean removeComment(Comment comment) {
		return this.commentList.removeComment(comment);
	}

	@Override
	public void updateComment(Comment comment) {
		this.commentList.updateComment(comment);
	}

	@Override
	public List<Comment> getAllComments() {
		return this.commentList.getAllComments();
	}

	@Override
	public Follow follow(User user) {
		return this.followList.follow(user);
	}

	@Override
	public boolean unfollow(Follow follow) {
		return this.followList.unfollow(follow);
	}

	@Override
	public Follow unfollow(User user) {
		return this.followList.unfollow(user);
	}

	@Override
	public List<Follow> getAllFollower() {
		return this.followList.getAllFollower();
	}

	@Override
	public Like like(User user) {
		return this.likeList.like(user);
	}

	@Override
	public boolean unlike(Like like) {
		return this.likeList.unlike(like);
	}

	@Override
	public Like unlike(User user) {
		return this.likeList.unlike(user);
	}

	@Override
	public List<Like> getAllLikes() {
		return this.likeList.getAllLikes();
	}

	/**
	 * 
	 * @param user
	 * @param text
	 * @return the answer object or null if this is not a question
	 */
	public Answer addAnswer(User user, String text) {
		Answer answer = new Answer(text, user, this);
		this.answers.add(answer);
		return answer;
	}

	/**
	 * 
	 * @param answer
	 * @return true if the answer was removed, false if this is no question or the
	 *         answer is not part of this question.
	 */
	public boolean removeAnswer(Answer answer) {
		return this.answers.remove(answer);
	}

	/**
	 * 
	 * @param answer
	 * @return true if the update worked and false if this is a question or no
	 *         answer was found which could be updated
	 */
	public boolean updateAnswer(Answer answer) {
		Answer found = this.answers.stream().filter(a -> a.equals(answer)).findFirst().get();
		if (found == null) {
			return false;
		}
		found.copyAttributes(answer);
		return true;
	}

	/**
	 * get all answers
	 * 
	 * @return returns null if this is not a question, otherwise a clone of all the
	 *         answers
	 */
	public List<Answer> getAllAnswers() {
		return ModelFactory.getFactory().getListClone(this.answers);
	}

	/**
	 * does only implement the copy for the answer object
	 */
	@Override
	public void copyAttributes(Question source) {
		this.setCreated(source.getCreated());
		this.setOwner(source.getOwner());
		this.setText(source.getText());
	}

	@Override
	public String getType() {
		return TYPE;
	}

}
