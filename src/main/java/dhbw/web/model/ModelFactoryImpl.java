package dhbw.web.model;

import java.util.ArrayList;
import java.util.List;

import dhbw.web.interfaces.ModelFactory;

/**
 * provide implementations for the common interfaces
 * 
 * @author nivogel
 *
 */
public class ModelFactoryImpl implements ModelFactory {

	/**
	 * create a new list
	 * 
	 * @return new list
	 */
	public <T> List<T> getList() {
		return new ArrayList<>();
	}

	/**
	 * create a clone of a list
	 * 
	 * @param elements copy all references from this list
	 * @return a new list with the same elements
	 */
	public <T> List<T> getListClone(List<T> elements) {
		return new ArrayList<>(elements);
	}
}
